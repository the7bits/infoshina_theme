# -*- coding: utf-8 -*-
from django.template import Library

register = Library()

@register.filter(name='correct_variants')
def correct_variants(product):
    # import pdb
    # pdb.set_trace()
    if product.parent is None:
        variants = product.get_variants()
    else:
        variants = product.parent.get_variants()
    variants = variants.exclude(id=product.id)
    return variants

@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_title(context, block_super):
    title_min_max = u'Купить автомобильные {type} цена от {min}\
 до {max} в интернет-магазине Infoshina'
    title_min = u'Купить автомобильные {type} цена от {min}\
 в интернет-магазине Infoshina'
    title_max = u'Купить автомобильные {type} цена до {max}\
 в интернет-магазине Infoshina'
    title = None
    _type = u'шины'
    if 'price' in context['unpacked_hr_urls']:
        for _filter in context['unpacked_hr_urls'].split('&'):
            if 'diski' in context['category_slug']:
                _type = u'диски'
            elif context['category_slug'] == 'shiny':
                _type = u'шины'
            if 'price' in _filter:
                _min, _max = _filter.split('=')[-1].split('..')
                if _min and _max:
                    title = title_min_max.format(min=_min, max=_max,
                        type=_type)
                elif _min:
                    title = title_min.format(min=_min, type=_type)
                elif _max:
                    title = title_max.format(max=_max, type=_type)
    return {
        'custom_seo': title or block_super
    }
@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_keywords(context, block_super):
    keywords_min_max = u'диски цена от {min}\
 до  {max}, купить диски цена от {min}, диски цена до {max} цена, интернет-магазин, infoshina.com.ua'
    keywords_min = u'диски цена от {min}\
 , купить диски цена от {min}, интернет-магазин, infoshina.com.ua'
    keywords_max = u'диски цена до  {max}\
 , диски цена до {max} цена, интернет-магазин, infoshina.com.ua'
    keywords_min_max_shiny = u'шины цена от {min}\
 до  {max}, купить резину цена от {min}, покрышки цена до {max} цена, интернет-магазин, infoshina.com.ua'
    keywords_min_shiny = u'шины цена от {min}\
 , купить резину цена от {min}, интернет-магазин, infoshina.com.ua'
    keywords_max_shiny = u'шины цена до {max}\
 , покрышки цена до {max} цена, интернет-магазин, infoshina.com.ua'
    keywords = None
    if 'price' in context['unpacked_hr_urls']:
        for _filter in context['unpacked_hr_urls'].split('&'):
            if 'diski' in context['category_slug']:
                if 'price' in _filter:
                    _min, _max = _filter.split('=')[-1].split('..')
                    if _min and _max:
                        keywords = keywords_min_max.format(min=_min, max=_max)
                    elif _min:
                        keywords = keywords_min.format(min=_min)
                    elif _max:
                        keywords = keywords_max.format(max=_max)
            elif context['category_slug'] == 'shiny':
                if 'price' in _filter:
                    _min, _max = _filter.split('=')[-1].split('..')
                    if _min and _max:
                        keywords = keywords_min_max_shiny.format(min=_min, max=_max)
                    elif _min:
                        keywords = keywords_min_shiny.format(min=_min)
                    elif _max:
                        keywords = keywords_max_shiny.format(max=_max)
    return {
        'custom_seo': keywords or block_super
    }
@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_description(context, block_super):
    description_min_max = u'Хотите купить {type} ценой от {min}\
 до {max} с доставкой по Украине? Заходите к нам в интернет-магазин - infoshina.com.ua'
    description_min = u'Хотите купить {type} ценой от {min}\
 с доставкой по Украине? Заходите к нам в интернет-магазин - infoshina.com.ua'
    description_max = u'Хотите купить {type} ценой до {max}\
 с доставкой по Украине? Заходите к нам в интернет-магазин - infoshina.com.ua'
    description = None
    _type = u'резину автомобильную'
    if 'price' in context['unpacked_hr_urls']:
        for _filter in context['unpacked_hr_urls'].split('&'):
            if 'diski' in context['category_slug']:
                _type = u'диски автомобильные'
            elif context['category_slug'] == 'shiny':
                _type = u'резину автомобильную'
            if 'price' in _filter:
                _min, _max = _filter.split('=')[-1].split('..')
                if _min and _max:
                    description = description_min_max.format(min=_min, max=_max,
                        type=_type)
                elif _min:
                    description = description_min.format(min=_min, type=_type)
                elif _max:
                    description = description_max.format(max=_max, type=_type)
    return {
        'custom_seo': description or block_super
    }
@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_h1(context, category):
    h1_min_max = u'{type} цена от {min} до {max}'
    h1_min = u'{type} цена от {min}'
    h1_max = u'{type} цена до {max}'
    h1 = None
    _type = category.name
    if 'price' in context['request'].META['QUERY_STRING']:
        for _filter in context['request'].META['QUERY_STRING'].split('&'):
            if category.slug == 'diski':
                _type = u'Диски'
            elif category.slug == 'shiny':
                _type = u'Шины'
            if 'price' in _filter:
                _min, _max = _filter.split('=')[-1].split('..')
                if _min and _max:
                    h1 = h1_min_max.format(min=_min, max=_max,
                        type=_type)
                elif _min:
                    h1 = h1_min.format(min=_min, type=_type)
                elif _max:
                    h1 = h1_max.format(max=_max, type=_type)
    return {
        'custom_seo': h1 or category.name
    }

